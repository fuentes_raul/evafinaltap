package root.model.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-10T19:05:28")
@StaticMetamodel(Palabras.class)
public class Palabras_ { 

    public static volatile SingularAttribute<Palabras, String> significado;
    public static volatile SingularAttribute<Palabras, Date> fecha;
    public static volatile SingularAttribute<Palabras, String> palabra;

}