
<%@page import="root.model.entities.Palabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Palabras palabras = (Palabras) request.getAttribute("palabras");

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body  class="text-center">
        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            <div class="inner">

            </div>
        </header> 
    </div>   
    <h2>Editar:</h2>

    <p class="">

    <form name="form" action="controllerPalabra" method="POST">
        <div class="form-group">
            <label for="palabras">Palabras:</label><br>
            <input name="palabras" value="<%=palabras.getPalabra()%>" class="form-control" required id="palabras"      />
        </div>

        <div class="form-group">
            <label for="significado">Significado:</label><br>
            <input atep="any" name="significado" value="<%=palabras.getSignificado()%>" class="form-control" required id="significado"      />
            <br>
            <div class="form-group">
                <label for="fecha">Fecha:</label><br>
                <input name="fecha" input type="datetime-local" class="form-control" value="<%=palabras.getFecha()%>" class="form-control" required id="fecha"      />
                <br>
                <button type="submit" name="accion" value="grabarEditar" class="btn btn-success">Grabar</button>
                <button type="submit" name="accion" value="SalirEditar" class="btn btn-success">Salir</button>

                </form>
                </body>
                </html>
