

<%@page import="root.model.entities.Palabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body  class="text-center">
        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            <header class="mast mb-auto">
                <div class="inner">
                </div>
            </header>
        </div>   
        <main role="main" class="inner cover">
            <h2 class="cover-heading"></h2>
              <h2>Crear:</h2>
            <p class="">
              
            <form name="form" action="controllerPalabra" method="POST">
                <div class="form-group">
                    <label for="palabras">Palabra: </label><br>
                    <input name="palabras" class="form-control" required id="palabras" aria-describedby="palabrasHelp" />
                </div>

                <div class="form-group">
                    <label for="significado">Significado: </label><br>
                    <input name="significado" class="form-control" required id="significado" aria-describedby="significadoHelp" />
                    <br>
                    <div class="form-group">
                        <label for="fecha">Fecha: </label><br>
                        <input name="fecha" class="form-control" required id="fecha" aria-describedby="fechaHelp" />
                        <br>
                        
                            <button type="submit" type="submit" name="accion" value="grabarCrear" class="btn btn-success">Grabar</button>
                            <button type="submit" class="btn btn-success">Salir</button>
                            


                            </form>

                            </main>

                            </body>
                            </html>
