/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.PalabrasDAO;
import root.model.entities.Palabras;

/**
 *
 * @author ANDRES
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String palabras = request.getParameter("palabras");
        String significado = request.getParameter("significado");
        String fecha = request.getParameter("fecha");
       
        Palabras palabra = new Palabras();
        palabra.setPalabra(palabras);
        palabra.setSignificado(significado);
        palabra.setFecha(Date.from(Instant.now()));
       

        PalabrasDAO dao = new PalabrasDAO();
        try {
            dao.create(palabra);
            Logger.getLogger("log").log(Level.INFO, "Valor identificacion palabra:{0}", palabra.getPalabra());
        } catch (Exception ex) {
            Logger.getLogger("log").log(Level.SEVERE, "Se presenta un error al ingresar esa palabra.{0}", ex.getMessage());
        }

        request.getRequestDispatcher("salida.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PalabrasDAO dao = new PalabrasDAO();
        List<Palabras> palabras = dao.findPalabrasEntities();
        System.out.println("Cantidad palabras en BD"+palabras.size());
        request.setAttribute("palabras", palabras);
        request.getRequestDispatcher("salida.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
